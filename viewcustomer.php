<!DOCTYPE html>
<html>
	<?php include 'db.php';
	$sql = "select * from profiles";
	$rows=$db->query($sql);
	?>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
		<style>  body{
		background-image:url("img1.png") ;
		} </style>
		<title>viewcustomer</title>
		<style>
		table {
		border-collapse: collapse;
		width: 100%;
		}
		th, td {
		padding: 8px;
		text-align: left;
		border-bottom: 1px solid #ddd;
		}
		tr:hover {background-color:#f5f5f5;}
		</style>
	</head>
	<body>
		
	<H1 class="pull-right">צפייה בפרטי לקוחות</H1>
	<br>
	<br>
	<br>
	<br>
	<td><a href="index.php" class="btn btn-primary btn-lg pull-right">ראשי</a> </td>
	<td><a href="gallery.php" class="btn btn-primary btn-lg pull-right">גלריה</a> </td>
	<td><a href="Queues.php" class="btn btn-primary btn-lg pull-right">תורים</a> </td>
	<button value="print" type ="button"  class="btn btn-dark" onclick="print()" >הדפסה</button>
	<br>
	<br>
	<br>
	<br>
	<div class="container">
		<table>
			<thead>
				<tr>
					<th>שם</th>
					<th>אימייל</th>
					<th>טלפון</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<?php while($row = $rows->fetch_assoc()): ?>
					<td class="col-md-3"><?php echo $row['name']; ?></td>
					<td class="col-md-3"><?php echo $row['Email']; ?></td>
					<td class="col-md-3"><?php echo $row['phone']; ?></td>
					<td><a href="changeprofile.php?id=<?php echo $row['id'];?>" class="btn btn-success">עריכה</a> </td>
					<td><a href="removecustomer.php?id=<?php echo $row['id'];?>" class="btn btn-danger">מחיקה</a> </td>
				</tr>
				<?php endwhile; ?>
			</tbody>
		</table>
	</div>
</body>
</html>